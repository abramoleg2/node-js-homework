const path = require('path');

function checkExtenion(filename) {
    const supportFormats = [".log", ".txt", ".json", ".yaml", ".xml", ".js"];
    if (supportFormats.includes(path.extname(filename))) {
        return true;
    }
    if (supportFormats.includes(filename)) {
        return true;
    }
    return false;
}

module.exports = checkExtenion;