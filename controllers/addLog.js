const fs = require("fs");
const path = require("path");

function addRequestToLog(request) {
    let logPath = path.join(path.dirname(__dirname), "logs.log");
    fs.appendFile(logPath, `{requestUrl:${request.url}, requestMethod:${request.method}}\n`, (err) => {
        if (err) {
            console.log("Failed to write request parameters to log");
        }
    });



}

module.exports = addRequestToLog;