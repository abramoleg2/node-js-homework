const path = require('path');
const checkExstention = require("./checkExstentionType");

function checkRequiredParams(body) {
    const regex = /^([a-zA-Z0-9]*\.*[a-zA-Z0-9-]*)*$/;
    let fileName = body.filename;
    let name = path.parse(fileName).name;
    let extention = path.parse(fileName).ext;
    let isTypeTrue = checkExstention(extention);
    if (regex.test(name) && isTypeTrue) {
        return true;
    }
    return false;
}

module.exports = checkRequiredParams;