
const express = require("express");
const fs = require("fs");
const path = require("path");
const checkRequiredParams = require("./controllers/checkRequiredParams");
const addRequestToLog = require("./controllers/addLog");
const UploadedDates = require("./helpers/uploadedDates");
const apiDir = path.join(__dirname, "api");
const filesDir = path.join(__dirname, "api", "files");

let uploadedDates = new UploadedDates();
const myApp = express();
myApp.use(express.json());

try {
  if (!fs.existsSync(apiDir)) {
    fs.mkdirSync(apiDir);
    fs.mkdirSync(filesDir);
  } else {
    if (!fs.existsSync(filesDir)) {
      fs.mkdirSync(filesDir);
    }
  }
} catch (err) {
  throw err;
}

myApp.post("/api/files", (request, response) => {
  addRequestToLog(request);
  if (checkRequiredParams(request.body)) {
    if (request.body.content === "") {
      return response.status(400).json({
        message: "Please specify 'content' parameter"
      });
    } else {
      let filename = path.join(__dirname, request.url, request.body.filename);
      fs.open(filename, "wx", (err) => {
        if (err) {
          return response.status(500).json({
            message: "Server error"
          });
        } else {
          let content = request.body.content;
          fs.writeFile(filename, content, function (err) {
            if (err) {
              return response.status(500).json({
                message: "Server error"
              });
            }
            let name = request.body.filename;
            let date = new Date().toLocaleString();
            uploadedDates.addFileUploadDate(name, date);
            response.status(200).json({
              message: "File created successfully"
            });
          });
        }
      });
    }
  } else {
    response.status(400).json({
      message: "Missing parameters or incorrect file extension"
    });
  }
})

myApp.get("/api/files", (request, response) => {
  addRequestToLog(request);
  fs.readdir(path.join(__dirname, request.url), (err, uploadedList) => {
    if (err) {
      return response.status(500).json({
        message: "Server error"
      });
    } else if (uploadedList.lenth === 0) {
      return response.status(400).json({
        message: "Client error"
      });
    }
    response.status(200).json({
      message: "Success",
      files: uploadedList
    });
  });
})

myApp.get("/api/files/:filename", (request, response) => {
  addRequestToLog(request);
  if (checkRequiredParams({ filename: request.params.filename })) {
    let currentName = request.params.filename;
    fs.open(path.join(__dirname, request.url), "r", (err) => {
      if (err) {
        return response.status(400).json({
          message: `No file with ${currentName} filename found`
        });
      } else {
        fs.readFile(path.join(__dirname, request.url), "utf-8", function (err, data) {
          if (err) {
            return response.status(500).json({
              message: "Server error"
            });
          } else {
            let name = request.params.filename;
            uploadedDates.getFileUploadDate(name);
            response.status(200).json({
              message: "Success",
              filename: path.basename(request.url),
              content: data,
              extension: path.parse(request.url).ext,
              uploadedDate: new Date().toLocaleString()
            });
          }
        });
      }
    });
  } else {
    response.status(400).json({
      message: "Wrong name or incorrect file extension"
    })
  }
})

myApp.listen(8080, (request, response) => {
  console.log("Serever started on port 8080...");
});