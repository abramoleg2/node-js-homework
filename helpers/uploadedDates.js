
class UploadedDates {
    dates = {};

    addFileUploadDate(prop, val) {
        this.dates[prop] = val;
    }

    getFileUploadDate(key) {
        for (let prop in this.dates) {
            if (key === prop) {
                return this.dates[key];
            }
        }
    }
}

module.exports = UploadedDates;